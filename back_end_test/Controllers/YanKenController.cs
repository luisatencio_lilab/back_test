﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back_end_test.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace back_end_test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class YanKenController : ControllerBase
    {
        // GET: api/<YanKenController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "PIEDRA", "PAPEL","TIJERA" };
        }

        // GET api/<YanKenController>/5
        [HttpGet("{mano}")]
        public ActionResult<string> Get(MANO mano)
        {
            YanKenPon yanKen = new YanKenPon();

            return Ok(yanKen.jugar(mano));
        }

        // POST api/<YanKenController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<YanKenController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<YanKenController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
