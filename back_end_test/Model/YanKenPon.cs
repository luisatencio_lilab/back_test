﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back_end_test.Model
{
    public enum MANO
    {
        PIEDRA,
        PAPEL,
        TIJERA,
    }
    public class YanKenPon
    {
        public MANO _mano { get; set; }
        
        public YanKenPon()
        {
            var rng = new Random();
            _mano = (MANO)(rng.Next(0, 3));
        }

        public string jugar(MANO jugador)
        {
            string resultado = "PERDISTE";

            switch (jugador)
            {
                case MANO.PIEDRA:
                    if (_mano == MANO.PIEDRA)
                        resultado = "EMPATE";
                    if (_mano == MANO.PAPEL)
                        resultado = "PEDISTE";
                    if (_mano == MANO.TIJERA)
                        resultado = "GANASTE";
                    break;
                case MANO.PAPEL:
                    if (_mano == MANO.PIEDRA)
                        resultado = "GANASTE";
                    if (_mano == MANO.PAPEL)
                        resultado = "EMPATE";
                    if (_mano == MANO.TIJERA)
                        resultado = "PERDISTE";
                    break;
                case MANO.TIJERA:
                    if (_mano == MANO.PIEDRA)
                        resultado = "PERDISTE";
                    if (_mano == MANO.PAPEL)
                        resultado = "EMPATE";
                    if (_mano == MANO.TIJERA)
                        resultado = "GANASTE";
                    break;
                default:
                    resultado = "PERDISTE";
                    break;
            }
            return resultado;
        }
    }
}
